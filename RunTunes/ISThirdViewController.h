//
//  ISThirdViewController.h
//  RunTunes
//
//  Created by Wes Anderson on 2014-11-07.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

@import CoreData;
@import UIKit;
@import Foundation;
@import AVFoundation;

#import "AppDelegate.h" // to grab managedObjectContext
#import "UserProfile.h" // CoreData class
#import "Pedometer.h" // CoreData class + added functionality
#import <CoreLocation/CoreLocation.h>

@interface ISThirdViewController : UIViewController <AVAudioPlayerDelegate, CLLocationManagerDelegate>

@property (nonatomic, retain) CLLocationManager *locationManager;

@end
