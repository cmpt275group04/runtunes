//
//  EndRunViewController.h
//  RunTunes
//
//  Created by Paul Khuu on 2014-11-16.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface EndRunViewController : UIViewController <CPTPlotDataSource>

//@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hostView;

- (IBAction)saveScreenshot;
- (IBAction)postToTwitter:(id)sender;
- (IBAction)postToFacebook:(id)sender;

@end
