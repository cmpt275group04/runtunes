//
//  ISFirstViewController.h
//  RunTunes
//
//  Created by Wes Anderson on 2014-11-07.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

@import Foundation; // Used for NSScanner
@import CoreData;
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UserProfile.h"

@interface ISFirstViewController : UIViewController

@end
