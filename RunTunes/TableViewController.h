//
//  TableViewController.h
//  TableViewTest
//
//  Created by luke zeng on 2014-11-16.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAQViewController.h"
#import "TipsViewController.h"
#import "RepErrViewController.h"

@interface TableViewController : UITableViewController

@end
