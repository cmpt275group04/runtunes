//
//  SongsViewController.h
//  MusicPlayer
//
//  Created by Paul Khuu on 2014-11-09.
//  Copyright (c) 2014 Paul Khuu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface SongsViewController : UITableViewController

@end
