//
//  TempPedometerData.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-20.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "TempPedometerData.h"

@implementation TempPedometerData

- (id)init{
    self.table = [[NSMutableArray alloc] init];
    self.tableEntry = [[NSArray alloc] initWithObjects:@"", @"", nil];
    self.count = 0;
    
    return self;
}

// This method is called every time the CMPedometer object receives an update from the hardware pedometer
// Called in Pedometer.m, startLivePedometerUpdatesToTemp
- (void)saveStepCount:(NSNumber *)stepCount AtTime:(NSDate *)date{
    // Create the data entry
    NSArray *dataEntry = [[NSArray alloc] initWithObjects:date, stepCount, nil];
    
    [self.table addObject:dataEntry]; // Add dataEntry to table
    self.count++; // Increment count
}

- (NSArray *)retreiveEntryAtPosition:(int)index{
    return self.table[index];
}

@end

//   timeStamp  |  stepCount
//   (index 0)  |  (index 1)
// ===========================
//  timeStamp1  |  stepCount1
// -------------|-------------
//  timeStamp2  |  stepCount2
// -------------|-------------
//  timeStamp3  |  stepCount3
//      .       |       .
//      .               .
//      .               .