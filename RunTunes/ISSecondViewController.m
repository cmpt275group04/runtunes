//
//  ISSecondViewController.m
//  RunTunes
//
//  Created by Wes Anderson on 2014-11-07.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "ISSecondViewController.h"

@interface ISSecondViewController ()

@end

@implementation ISSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Hide the back button
    if (self) {
        self.navigationItem.hidesBackButton = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
