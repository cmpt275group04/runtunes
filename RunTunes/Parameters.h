//
//  Parameters.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-19.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#ifndef RunTunes_Parameters_h
#define RunTunes_Parameters_h

// Accessed from: RunProfile.m
// Sets the min and max bounds for the cadence tracked in-app
extern int const MIN_CADENCE; // steps/minute
extern int const MAX_CADENCE; // steps/minute

// Accessed from: ISFirstViewController.m
// Sets the span of each entry in the UserProfile.cadenceTable
// Ex. MIN_CADENCE = 100, cadenceEntrySpan = 5; cadenceTable entries: 100, 105, 110, etc...
extern int const paramCadenceEntrySpan;

// Accessed from: ISThirdViewController.m
// paramCycleLengthInSeconds specifies how long to test each cycle
// BPM array specifies the BPM values to test during the inital setup.  Can add values to increase the number of cycles?
extern int const BPMArray[];
extern int const BPMArrayCount;
extern int const paramCycleLengthInSeconds;

#endif
