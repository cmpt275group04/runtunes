//
//  NewActivityViewController.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "NewActivityViewController.h"
#import "ActiveActivityViewController.h"
#import "UserProfile.h"

@interface NewActivityViewController ()

@property (strong, nonatomic) IBOutlet UISegmentedControl *goalTypeSegmentedControl;
@property (strong, nonatomic) IBOutlet UILabel *primaryLabel;
@property (strong, nonatomic) IBOutlet UITextField *primaryField;
@property (strong, nonatomic) IBOutlet UILabel *secondaryLabel;
@property (strong, nonatomic) IBOutlet UITextField *secondaryField;
@property BOOL distanceTimeSelected;
@property UserProfile *user;

@end

@implementation NewActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.goalTypeSegmentedControl.selectedSegmentIndex = 0;
    self.distanceTimeSelected = YES;
    self.primaryLabel.text = @"Distance:";
    self.primaryField.placeholder = @"km";
    [self.secondaryLabel setHidden:NO];
    [self.secondaryField setHidden:NO];
    self.user = [[UserProfile alloc] initFromExistingUserProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//dismiss keyboard
- (BOOL)textFieldShouldReturn:(UITextField *) textField;{
    [textField resignFirstResponder];
    return YES;
}

//dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (IBAction)goalTypeChanged:(id)sender {
    if(self.goalTypeSegmentedControl.selectedSegmentIndex == 0){
        // Distance/Time selected
        self.distanceTimeSelected = YES;
        self.primaryLabel.text = @"Distance:";
        if([self.user.prefersMinPerKmPace isEqualToNumber:[NSNumber numberWithBool:YES]]){
            self.primaryField.placeholder = @"km";
        }
        else{
            self.primaryField.placeholder = @"miles";
        }
        
        [self.secondaryLabel setHidden:NO];
        [self.secondaryField setHidden:NO];
    }
    else{
        // Pace selected
        self.distanceTimeSelected = NO;
        self.primaryLabel.text = @"Pace:";
        
        if([self.user.prefersMinPerKmPace isEqualToNumber:[NSNumber numberWithBool:YES]]){
            self.primaryField.placeholder = @"min/km";
        }
        else{
            self.primaryField.placeholder = @"min/mile";
        }
        
        [self.secondaryLabel setHidden:YES];
        [self.secondaryField setHidden:YES];
    }
}

- (IBAction)button:(id)sender {
    BOOL validInput = NO;
    
    if(self.goalTypeSegmentedControl.selectedSegmentIndex == 0){
        // Distance/Time selected
        if([self.primaryField.text isEqualToString:@""]){
            [self standardAlertWithTitle:@"Missing Information"
                             WithMessage:@"Please enter a value in the Distance field"];
        }
        else if (![self isNumeric:self.primaryField.text]){
            [self standardAlertWithTitle:@"Invalid Input"
                             WithMessage:@"Please enter a number in the Distance field"];
        }
        else if([self.secondaryField.text isEqualToString:@""]){
            [self standardAlertWithTitle:@"Missing Information"
                             WithMessage:@"Please enter a value in the Time field"];
        }
        else if (![self isNumeric:self.secondaryField.text]){
            [self standardAlertWithTitle:@"Invalid Input"
                             WithMessage:@"Please enter a number in the Time field"];
        }
        else{
            validInput = YES;
        }
    }
    else{
        // Pace selected
        if([self.primaryField.text isEqualToString:@""]){
            [self standardAlertWithTitle:@"Missing Information"
                             WithMessage:@"Please enter a value in the Pace field"];
        }
        else if (![self isNumeric:self.primaryField.text]){
            [self standardAlertWithTitle:@"Invalid Input"
                             WithMessage:@"Please enter a number in the Pace field"];
        }
        else{
            validInput = YES;
        }
    }
    
    if(validInput){
        [self performSegueWithIdentifier:@"beginActivitySegue" sender:sender];
    }
}

- (void)standardAlertWithTitle:(NSString *)title WithMessage:(NSString *)msg{
    //push alert to user when open this view controller
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    //ok to continute
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"ok action")
                                                 style: UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {NSLog(@"ok action");}];
    //add "ok"action
    [alert addAction:ok];
    //show alert
    [self presentViewController:alert animated:YES completion:nil];
}

// Used to confirm the textfield entries are numbers only
-(BOOL)isNumeric:(NSString *)string{
    NSScanner *sc = [NSScanner scannerWithString:string];
    if ( [sc scanFloat:NULL] )
    {
        return [sc isAtEnd];
    }
    return NO;
}

-(int)calculateBPMfromPace:(double)pace{
    double newPace;
    double requestedBPM;
    UserProfile *user = [[UserProfile alloc] initFromExistingUserProfile];
    
    // Find closest entries in the RunProfile to extrapolate necessary BPM
    NSString *lowerCadenceKey = [self findNextGreatestEntryInCadenceTableLessThanPace:pace];
    NSString *higherCadenceKey = [self findNextLowestEntryInCadenceTableGreaterThanPace:pace];
    
    // If the requested pace does not have a higher and lower entry in the RunProfile, calculate the two lower/higher to extrapolate
    if(lowerCadenceKey == nil && higherCadenceKey != nil){ // if there is no entry in the RunProfile for a pace less than the requested pace
        lowerCadenceKey = higherCadenceKey;
        newPace = pace;
        while (lowerCadenceKey == higherCadenceKey){
            newPace -= 0.05;
            higherCadenceKey = [self findNextLowestEntryInCadenceTableGreaterThanPace:newPace];
        }
    }
    else if(higherCadenceKey == nil && lowerCadenceKey != nil){ // if there is no entry in the RunProfile for a pace greater than the requested pace
        higherCadenceKey = lowerCadenceKey;
        newPace = pace;
        while(higherCadenceKey == lowerCadenceKey){
            newPace += 0.05;
            lowerCadenceKey = [self findNextGreatestEntryInCadenceTableLessThanPace:newPace];
        }
    }
    else if (higherCadenceKey == nil && lowerCadenceKey == nil){
        NSLog(@"Fatal error occurred");
        //exit(EXIT_FAILURE);
    }
    
    struct dataPoint lowerCadencePoint;
    struct dataPoint higherCadencePoint;
    
    CadenceEntry *lower = [user.runProfile.cadenceTable valueForKey:lowerCadenceKey];
    lowerCadencePoint.XCoordinateCadence = [lowerCadenceKey floatValue];
    lowerCadencePoint.YCoordinatePace = [lower.averagePace floatValue];
    
    CadenceEntry *higher = [user.runProfile.cadenceTable valueForKey:higherCadenceKey];
    higherCadencePoint.XCoordinateCadence = [higherCadenceKey floatValue];
    higherCadencePoint.YCoordinatePace = [higher.averagePace floatValue];
    
    struct lineEquation line = [self getLineEquationFromLowerPoint:lowerCadencePoint HigherPoint:higherCadencePoint];
    
    requestedBPM = (pace - line.b)/line.m;
    
    NSLog(@"LowerCadencePoint: (pace, cadence) = (%f, %f)", lowerCadencePoint.XCoordinateCadence, lowerCadencePoint.YCoordinatePace);
    NSLog(@"HigherCadencePoint: (pace, cadence) = (%f, %f)", higherCadencePoint.XCoordinateCadence, higherCadencePoint.YCoordinatePace);
    NSLog(@"requestedBPM = (pace - line.b)/line.m ---> %f = (%f - %f)/%f", requestedBPM, pace, line.b, line.b);
    
    return (int)(requestedBPM + 0.5);
}

-(NSString *)findNextGreatestEntryInCadenceTableLessThanPace:(double)pace{
    double currentGreatest = -1;
    NSString *currentGreatestKey = nil;
    UserProfile *user = [[UserProfile alloc] initFromExistingUserProfile];

    for(NSString *key in user.runProfile.cadenceTable){
        CadenceEntry *entry = [user.runProfile.cadenceTable valueForKey:key];
        
        if(entry.averagePace != nil){
            double paceFromKey = [entry.averagePace doubleValue];
            
            if(paceFromKey > pace){
                if (currentGreatest == -1){
                    currentGreatest = paceFromKey;
                    currentGreatestKey = key;
                }
                else{
                    if([key integerValue] > [currentGreatestKey integerValue]){
                        currentGreatest = paceFromKey;
                        currentGreatestKey = key;
                    }
                }
            }
        }
    }
    
    return currentGreatestKey;
}

-(NSString *)findNextLowestEntryInCadenceTableGreaterThanPace:(double)pace{
    int currentLowest = -1;
    NSString *currentLowestKey;
    UserProfile *user = [[UserProfile alloc] initFromExistingUserProfile];
    
    for(NSString *key in user.runProfile.cadenceTable){
        CadenceEntry *entry = [user.runProfile.cadenceTable valueForKey:key];
        
        if(entry.averagePace != nil){
            double paceFromKey = [entry.averagePace doubleValue];
            
            if(paceFromKey <= pace){
                if (currentLowest == -1){
                    currentLowest = paceFromKey;
                    currentLowestKey = key;
                }
                else{
                    if([key integerValue] < [currentLowestKey integerValue]){
                        currentLowest = paceFromKey;
                        currentLowestKey = key;
                    }
                }
            }
        }
    }
    
    return currentLowestKey;
}

-(struct lineEquation)getLineEquationFromLowerPoint:(struct dataPoint)lowerCadencePoint HigherPoint:(struct dataPoint)higherCadencePoint{
    struct lineEquation line;
    
    line.m = (higherCadencePoint.YCoordinatePace - lowerCadencePoint.YCoordinatePace)/(higherCadencePoint.XCoordinateCadence - lowerCadencePoint.XCoordinateCadence);
    line.b = (line.m * (-lowerCadencePoint.XCoordinateCadence)) + lowerCadencePoint.YCoordinatePace;
    
    return line;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"beginActivitySegue"])
    {
        double requestedPace;
        
        ActiveActivityViewController *controller = (ActiveActivityViewController *)segue.destinationViewController;
        
        if(self.goalTypeSegmentedControl.selectedSegmentIndex == 0){
            // Distance/Time selected
            requestedPace = [self.secondaryField.text floatValue]/[self.primaryField.text floatValue];
        }
        else{
            // Pace selected
            requestedPace = [self.primaryField.text floatValue];
        }
        
        
        
        NSLog(@"%d", [self calculateBPMfromPace:requestedPace]);
        controller.requestedBPM = [self calculateBPMfromPace:requestedPace];
    }
}

@end
