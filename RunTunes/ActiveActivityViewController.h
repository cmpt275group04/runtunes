//
//  ActiveActivityViewController.h
//  RunTunes
//
//  Created by Naoya on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import <MediaPlayer/MediaPlayer.h>





@interface ActiveActivityViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate, MPMediaPickerControllerDelegate> {
    IBOutlet UIButton *playPauseButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *BPMLabel;
    MPMusicPlayerController *MusicPlayer;
    CLLocation *oldLocation;
    double distanceTraveled;
}

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (nonatomic) NSTimer *duration;
@property BOOL STOP, firstWrite, isPaused;
@property NSFileHandle *fileHandle;
@property NSString *filePath;
@property (nonatomic) int timerCounter;
@property (nonatomic) int requestedBPM;

@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;

//   @property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
//    MPMusicPlayerController *MusicPlayer;



- (IBAction)stopActivity:(id)sender;
- (IBAction)startPause:(id)sender;

- (IBAction)previousSong:(id)sender;
- (IBAction)playPause:(id)sender;
- (IBAction)nextSong:(id)sender;


- (void) registerMediaPlayerNotifications;




@end
