//
//  RunProfile.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-16.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "RunProfile.h"
/*
// Transformer to allow persistant CoreData storage of RunProfile objects
@implementation RunProfileToDataTransformer

+ (BOOL)allowsReverseTransformation {
    return YES;
}

+ (Class)transformedValueClass {
    return [NSData class];
}

- (id)transformedValue:(id)value {
    
}

- (id)reverseTransformedValue:(id)value {
    
}

@end
*/

@implementation CadenceEntry

@synthesize stepCount;
@synthesize averagePace;

-(id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    self.stepCount = [decoder decodeObjectForKey:@"stepCount"];
    self.averagePace = [decoder decodeObjectForKey:@"averagePace"];
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.stepCount forKey:@"stepCount"];
    [encoder encodeObject:self.averagePace forKey:@"averagePace"];
}

@end


@implementation RunProfile

/* initializes the RunProfile object with (MIN_CADENCE + MAX_CADENCE)/range entries in cadenceTable.
 
 Access values with:
        NSString *key = @"100"; // e.g. 100 BPM
        cadenceEntry temp = [self.cadence objectForKey:key]
        temp.stepCount = ...
        temp.averagePace = ...
        
        NSValue *updatedEntry = [NSValue value:&temp withObjCType:@encode(cadenceEntry)];
        [self.cadence setObject:temp forKey:key]
*/

// The function initalizes the RunProfile object with self.entrySpan = width of each "bucket" of cadence values.
// It also initializes the NSMutableDictionary with tempCount number of key-value pairs.
// Each pair consists of NSString *cadenceValue (e.g. @"100") <---> cadenceEntry struct
// cadenceEntry struct contains int stepCount and double averagePace
- (id)initWithSpan:(int)span {
    if (self = [super init]) {
        self.cadenceTable = [[NSMutableDictionary alloc] init]; // initialize cadenceTable dictionary
        self.entrySpan = [NSNumber numberWithInt:span]; // set entrySpan value
        
        int tempCount = (MAX_CADENCE - MIN_CADENCE)/span; // determine number of entries in dictionary; not saved anywhere.  Use [self.cadenceTable count] instead
        
        // create empty entry to be copied to cadence dictionary
        //cadenceEntry emptyEntry;
        //emptyEntry.stepCount = 0; // *** necessary?
        //emptyEntry.averagePace = 0; // *** necessary?
        
        
        
        NSLog(@"RunProfile Initilized State:");
        NSLog(@"Key (Cadence)  |  Value (stepCount, averagePace)");
        
        // assign empty structure to dictionary for each necessary cadence value
        for (int i = 0; i < tempCount; i++){
            //NSValue *valueFromEmptyEntry = [NSValue value:&emptyEntry withObjCType:@encode(cadenceEntry)]; // Create empty cadenceEntry
            CadenceEntry *emptyEntry = [[CadenceEntry alloc] init];
            
            NSString *cadenceValueKey = [NSString stringWithFormat:@"%i", MIN_CADENCE + i*span]; // Set cadenceValue
            [self.cadenceTable setObject:emptyEntry forKey:cadenceValueKey];
            
            CadenceEntry *testing = [self.cadenceTable valueForKey:cadenceValueKey];
            NSLog(@"%@  |  %@, %@", cadenceValueKey, testing.stepCount, testing.averagePace);
        }
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    self.cadenceTable = [decoder decodeObjectForKey:@"cadenceTable"];
    self.entrySpan = [decoder decodeObjectForKey:@"entrySpan"];
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.cadenceTable forKey:@"cadenceTable"];
    [encoder encodeObject:self.entrySpan forKey:@"entrySpan"];
}

@end