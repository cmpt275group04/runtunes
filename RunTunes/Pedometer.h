//
//  PedometerData.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

@import Foundation;
@import CoreData;
@import CoreMotion;
#import "TempPedometerData.h"

@interface Pedometer : NSObject

// Pedometer CoreData Entity Attributes
//@property (nonatomic, retain) NSNumber * stepCount;
//@property (nonatomic, retain) NSDate * timeStamp;

// Public Pedometer Class Properties
@property (nonatomic,strong) CMPedometer* pedometer;
@property (nonatomic) NSInteger* currentCadence;
@property (nonatomic) NSDate* timeOfLastQuery;

//- (void)startLivePedometerUpdatesWithManagedObjectContext:(NSManagedObjectContext *)moc;
- (void)startLivePedometerUpdatesToTemp:(TempPedometerData *)data;
- (void)stopPedometer;
//- (double)calculateAverageCadence:(NSDate *)fromDate;
//- (double)calculateInstantaneousCadence:(int)toleranceInSeconds;
- (double)calculateAverageCadenceFromTemp:(TempPedometerData *)tempData;
- (int)calculateStepsTakenFromTemp:(TempPedometerData *)tempData;

@end
