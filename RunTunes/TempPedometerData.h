//
//  TempPedometerData.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-20.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TempPedometerData : NSObject

@property NSMutableArray *table;
@property NSArray *tableEntry;
@property int count;

- (id)init;
- (void)saveStepCount:(NSNumber *)stepCount AtTime:(NSDate *)date;
- (NSArray *)retreiveEntryAtPosition:(int)index;

@end
