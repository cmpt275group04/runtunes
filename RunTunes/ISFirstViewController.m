//
//  ISFirstViewController.m
//  RunTunes
//
//  Created by Wes Anderson on 2014-11-07.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#define TESTING

#import "ISFirstViewController.h"
#import "RunProfile.h"
#import "Parameters.h"
//#import "ISThirdViewController.h"


@interface ISFirstViewController ()

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *weightKG;
@property (weak, nonatomic) IBOutlet UITextField *weightLBS;
@property (weak, nonatomic) IBOutlet UITextField *heightCM;
@property (weak, nonatomic) IBOutlet UITextField *heightFT;
@property (weak, nonatomic) IBOutlet UITextField *heightINCH;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *paceUnits;
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UISegmentedControl *weightUnit;
@property (strong, nonatomic) IBOutlet UISegmentedControl *heightUnit;
@property (nonatomic) BOOL kgShownNow;
@property (nonatomic) BOOL cmShownNow;

@end

@implementation ISFirstViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    // initial setup to display kg for weight and cm for height
    self.kgShownNow = true;
    self.cmShownNow = true;
    [self.weightLBS setHidden:TRUE];
    [self.heightFT setHidden:TRUE];
    [self.heightINCH setHidden:TRUE];
    
    // Grab managedObjectContext from AppDelegate
    self.managedObjectContext = ((AppDelegate *) [UIApplication sharedApplication].delegate).managedObjectContext;
    
    NSString *message = @"Before we get started we need to configure your User Profile.\n\nPlease enter the following information.";
    [self standardAlertWithTitle:@"Welcome to RunTunes!" WithMessage:message];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextButton:(id)sender {
    // Check for incomplete and/or erroneous fields
    
    
    // Check for incomplete fields
    if([self.name.text isEqualToString:@""]){
        [self standardAlertWithTitle:@"Missing Information"
                         WithMessage:@"Please enter a value in the Name field"];
    }
    else if ([self.age.text isEqualToString:@""]){
        [self standardAlertWithTitle:@"Missing Information"
                         WithMessage:@"Please enter a value in the Age field"];
    }
    else if (![self isNumeric:self.age.text]){
        [self standardAlertWithTitle:@"Invalid Input"
                         WithMessage:@"Please enter a number in the Age field"];
    }
    else if (self.kgShownNow && [self.weightKG.text isEqualToString:@""]){
        [self standardAlertWithTitle:@"Missing Information"
                         WithMessage:@"Please enter a value in the Weight field"];
    }
    else if (!(self.kgShownNow) && [self.weightLBS.text isEqualToString:@""]){
        [self standardAlertWithTitle:@"Missing Information"
                         WithMessage:@"Please enter a value in the Weight field"];
    }
    else if (self.kgShownNow && (![self isNumeric:self.weightKG.text])){
        [self standardAlertWithTitle:@"Invalid Input"
                         WithMessage:@"Please enter a number in the Weight field"];
    }
    else if (!(self.kgShownNow) && (![self isNumeric:self.weightLBS.text])){
        [self standardAlertWithTitle:@"Invalid Input"
                         WithMessage:@"Please enter a number in the Weight field"];
    }
    else if (self.cmShownNow && [self.heightCM.text isEqualToString:@""]){
        [self standardAlertWithTitle:@"Missing Information"
                         WithMessage:@"Please enter a value in the Height field"];
    }
    else if (!(self.cmShownNow) && ([self.heightFT.text isEqualToString:@""] || [self.heightINCH.text isEqualToString:@""])){
        [self standardAlertWithTitle:@"Missing Information"
                         WithMessage:@"Please enter a value in the Height field"];
    }
    else if (self.cmShownNow && ![self isNumeric:self.heightCM.text]){
        [self standardAlertWithTitle:@"Invalid Input"
                         WithMessage:@"Please enter a number in the Height field"];
    }
    else if (!(self.cmShownNow) && (![self isNumeric:self.heightFT.text] || ![self isNumeric:self.heightINCH.text])){
        [self standardAlertWithTitle:@"Invalid Input"
                         WithMessage:@"Please enter a number in the Height field"];
    }    else{
        // If in this block the user input is valid
        
        // Delete any existing user profiles //*** Make more rubust in future versions
        [self deleteAllObjects:@"UserProfile"];
        
        // Setup UserProfile managed object to be saved by CoreData
        UserProfile* myMO = (UserProfile *)[NSEntityDescription insertNewObjectForEntityForName:@"UserProfile"
                                                                         inManagedObjectContext:[self managedObjectContext]];
        
        // Split the Name field into firstName and lastName objects
        NSString *firstName = [[self.name.text componentsSeparatedByString:@" "] objectAtIndex:0];
        
        NSString *lastName = nil;
        if (self.name.text.length != firstName.length){
            lastName = [self.name.text substringFromIndex:(firstName.length+1)];
        }
        
        // Set managed object properties
        myMO.firstName = firstName;
        if (self.name.text.length != firstName.length){
            myMO.lastName = lastName;
        }
        myMO.age = [NSNumber numberWithInteger: [self.age.text integerValue]];
        
        //**************** Probably need CoreData modification for different unit values *************
        // Fixed the minor problem (originally storing integer but had it store double for these variables)
        if (self.kgShownNow){
            myMO.weight = [NSNumber numberWithDouble: [self.weightKG.text doubleValue]];
            myMO.prefersKgWeight = [NSNumber numberWithBool:YES];
        }
        else{
            myMO.weight = [NSNumber numberWithDouble: [self.weightLBS.text doubleValue]];
            myMO.prefersKgWeight = [NSNumber numberWithBool:NO];
        }
        if (self.cmShownNow){
            myMO.height = [NSNumber numberWithDouble: [self.heightCM.text doubleValue]];
            myMO.heightInch = [NSNumber numberWithDouble: 0.0]; // maybe not necessary just in case
            myMO.prefersCmHeight = [NSNumber numberWithBool:YES];
        }
        else{
            myMO.height = [NSNumber numberWithInteger: [self.heightFT.text integerValue]];
            myMO.heightInch = [NSNumber numberWithDouble: [self.heightINCH.text doubleValue]];
            myMO.prefersCmHeight = [NSNumber numberWithBool:NO];
        }
        //********************************************************************************************
        
        if ([[self gender] selectedSegmentIndex]){
            myMO.isMale = [NSNumber numberWithBool:NO]; // is female --- isMale = 0
        }
        else{
            myMO.isMale = [NSNumber numberWithBool:YES]; // is male --- isMale = 1
        }
        if([self.paceUnits selectedSegmentIndex]){
            myMO.prefersMinPerKmPace =[NSNumber numberWithBool:NO];
        }
        else{
            myMO.prefersMinPerKmPace =[NSNumber numberWithBool:YES];

        }
        
        // *** Testing required -- crashes app -- NOT ANY MORE. DONE?
        myMO.runProfile = [[RunProfile alloc] initWithSpan:paramCadenceEntrySpan];
        
        // Save data from managed object to CoreData and report error if one occurred
        NSError *saveError = nil;
        if(![[self managedObjectContext]save:&saveError]){
            // If a save error occurred
            NSLog(@"Error occured: %@", saveError);
        }
        
#ifdef TESTING
        // Create Core Data fetch request and fetch data
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSError *error = nil;
        NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) {
            NSLog(@"Error occured: %@", error);
        }
        
        // Grab the first element of the array
        UserProfile *user = fetchedObjects[0];
        
        // Output data for verification
        NSLog(@"***User Profile data consistency check***\n");
        NSLog(@"(Entered Data) | (Stored Data)");
        NSLog(@"First Name: %@ | %@", firstName, user.firstName);
        if (user.lastName != nil){
            NSLog(@"Last Name: %@ | %@", lastName, user.lastName);
        }
        NSLog(@"Age: %@ | %@", (NSNumber *)self.age.text, user.age);
        
        //**************** Probably need CoreData modification for different unit values *************
        if (self.kgShownNow){
            NSLog(@"KG Weight: %@ | %@", (NSNumber *)self.weightKG.text, user.weight);
            NSLog(@"prefersKgWeight: YES  |  %@", user.prefersKgWeight);
            
        }
        else{
            NSLog(@"LBS Weight: %@ | %@", (NSNumber *)self.weightLBS.text, user.weight);
            NSLog(@"prefersKgWeight: NO  |  %@", user.prefersKgWeight);

        }
        
        if (self.cmShownNow){
            NSLog(@"CM Height: %@ | %@", (NSNumber *)self.heightCM.text, user.height);
            NSLog(@"prefersCmHeight: Yes  |  %@", user.prefersCmHeight);
            //NSLog(@"prefersMinPerKmPace: YES  |  %@", user.prefersMinPerKmPace);
        }
        else{
            NSLog(@"FT Height: %@ | %@", (NSNumber *)self.heightFT.text, user.height);
            NSLog(@"INCH Height: %@ | %@", (NSNumber *)self.heightINCH.text, user.heightInch);
            NSLog(@"prefersCmHeight: NO  |  %@", user.prefersCmHeight);
            //NSLog(@"prefersMinPerKmPace: NO  |  %@", user.prefersMinPerKmPace);
        }
        //********************************************************************************************
        

        NSString *gender;
        if([user.isMale boolValue] == YES){
            gender = @"Male";
        }
        else{
            gender = @"Female";
        }
        NSLog(@"Gender: %@ | %@", [[self gender] titleForSegmentAtIndex:[[self gender] selectedSegmentIndex]], gender);
#endif

    }
    
    // Release keyboard code
    [self.name resignFirstResponder];
    [self.age resignFirstResponder];
    [self.weightKG resignFirstResponder];
    [self.heightCM resignFirstResponder];
    // following 3 lines are added
    [self.weightLBS resignFirstResponder];
    [self.heightFT resignFirstResponder];
    [self.heightINCH resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



// **********************************
// ***** Keyboard Customization *****
// **********************************

// Dismiss keyboard when user touches an unassigned area of the screen
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

// Called when the return button is pressed on the soft keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    // if the selected textField is the name field
    if([textField isEqual:[self name]]){
        [[self age] becomeFirstResponder]; // select the age field
    }
    
    return YES;
}



// ******************************
// ***** Additional Methods *****
// ******************************

- (void)standardAlertWithTitle:(NSString *)title WithMessage:(NSString *)msg{
    //push alert to user when open this view controller
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    //ok to continute
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"ok action")
                                                 style: UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {NSLog(@"ok action");}];
    //add "ok"action
    [alert addAction:ok];
    //show alert
    [self presentViewController:alert animated:YES completion:nil];
}

// Used to confirm the textfield entries are numbers only
-(BOOL)isNumeric:(NSString *)string{
    NSScanner *sc = [NSScanner scannerWithString:string];
    if ( [sc scanFloat:NULL] )
    {
        return [sc isAtEnd];
    }
    return NO;
}

// Used to clear existing UserProfile objects in the managedObjectContext
- (void) deleteAllObjects: (NSString *) entityDescription  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [_managedObjectContext deleteObject:managedObject];
#ifdef TESTING
        NSLog(@"%@ object deleted",entityDescription);
#endif
    }
    if (![_managedObjectContext save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}

// This method is triggered by changing kg/lbs segment cntrol
- (IBAction)changeWeightUnit:(id)sender {
    // if kg is shown now and the segment control button is pressed, it has to change the unit into lbs
    if (self.kgShownNow){
        [self.weightKG setHidden:true]; // hide kg
        [self.weightLBS setHidden:false]; // show lbs
        
        self.kgShownNow = false;
    }
    else{
        [self.weightKG setHidden:false]; // show kg
        [self.weightLBS setHidden:true]; // hide lbs
        self.kgShownNow = true;
    }
}

// This method is triggered by changing cm/ft:in segment cntrol
- (IBAction)changeHeightUnit:(id)sender {
    // if cm is shown now and the segment control button is pressed, it has to change the unit into ft/in
    if (self.cmShownNow){
        [self.heightCM setHidden:true]; // hide cm
        [self.heightFT setHidden:false]; // show ft
        [self.heightINCH setHidden:false]; // show inch
        self.cmShownNow = false;
    }
    else{
        [self.heightCM setHidden:false]; // show cm
        [self.heightFT setHidden:true]; // hide ft
        [self.heightINCH setHidden:true]; // hide inch
        self.cmShownNow = true;
    }
}


@end
