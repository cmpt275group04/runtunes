//
//  ActiveActivityViewController.m
//  RunTunes
//
//  Created by Naoya on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "ActiveActivityViewController.h"

@interface ActiveActivityViewController ()

@end

@implementation ActiveActivityViewController

@synthesize locationManager, mapView, fileHandle, filePath, firstWrite, isPaused, durationLabel, duration, timerCounter, musicPlayer, distanceLabel;

//@synthesize musicPlayer;

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    oldLocation = nil;
    distanceTraveled = 0.0;
    
    musicPlayer = [[MPMusicPlayerController alloc] init];
    
    // Pause and play
    if ([musicPlayer playbackState] == MPMusicPlaybackStatePlaying) {
        
        [playPauseButton setImage:[UIImage imageNamed:@"pauseButton.png"] forState:UIControlStateNormal];
        
    } else {
        
        [playPauseButton setImage:[UIImage imageNamed:@"playButton.png"] forState:UIControlStateNormal];
    }
    
    musicPlayer = [MPMusicPlayerController systemMusicPlayer];
    
    // Create instance of nowPlayingItem
    MPMediaItem *item = [musicPlayer nowPlayingItem];
    
    // Assign MPMediaItemPropertyTitle for song title to the label in storyboard
    NSString *songtitlestring = [item valueForProperty:MPMediaItemPropertyTitle];
    self->titleLabel.text = [NSString stringWithFormat:@"Title: %@",songtitlestring];
    
    // Assign MPMediaItemPropertyBeatsPerMinute for the bpm of the song to the label in storyboard
    NSNumber* bpmnumber = NULL;
    MPMediaItem *item2 = [musicPlayer nowPlayingItem];
    if (item2 != NULL) {
    bpmnumber = [item2 valueForProperty:MPMediaItemPropertyBeatsPerMinute];
    }
    
    // Convert to string for outputting
    NSString *bpmstring = [bpmnumber stringValue];
    self->BPMLabel.text = [NSString stringWithFormat:@"BPM: %@",bpmstring];
    
    [self refreshsongbpm];
    
    //[self fileWriteSetup];
    
    timerCounter = 0;
    [self timerStart];
    
    isPaused = NO;
    //firstWrite = TRUE;
    
    mapView.delegate = self; // set up a delegate for mapView. This is needed for drawing lines on the map
    mapView.mapType = MKMapTypeStandard;
    mapView.showsUserLocation = YES;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
    // check if the location service is available
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager.delegate = self; // set up a delegate for locationManager

        // start mearsurement for coordinates
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] && ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)) {
            [locationManager requestAlwaysAuthorization];
        }
        [self.locationManager startUpdatingLocation];
        
    }
    else {
        NSLog(@"Location services not available.");
    }
    // close the file
    //[fileHandle closeFile];
}


// Gets the song and bpm information and refreshes the outlets
- (void)refreshsongbpm {
    MPMediaItem *item = [musicPlayer nowPlayingItem];
    NSString *songtitlestring = [item valueForProperty:MPMediaItemPropertyTitle];
    self->titleLabel.text = [NSString stringWithFormat:@"Title: %@",songtitlestring];
    
    
    NSNumber* bpmnumber = NULL;
    MPMediaItem *item2 = [musicPlayer nowPlayingItem];
    
    if (item2 != NULL) {
        bpmnumber = [item2 valueForProperty:MPMediaItemPropertyBeatsPerMinute];
    }
    
    
    // Check for bpm range
    int bpmint = [bpmnumber intValue];
    if (_requestedBPM < bpmint - 5 || _requestedBPM > bpmint + 5 || bpmint == 0)
    {
        [musicPlayer skipToNextItem];
    }
    
    
    
    
    NSString *bpmstring = [bpmnumber stringValue];
    self->BPMLabel.text = [NSString stringWithFormat:@"BPM: %@",bpmstring];
}


#pragma mark - After a coordinate update
// This method is called when the user's location is updated
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *newLocation = [locations lastObject];
    
    // the following 3 lines are for increasing accuracy of distance traveled
    // If the updated location is considered not accurate enought, it each if statement will return this method immediatey
    // so 1:updating distanceTraveled, 2:writing the coordinate in the .txt file, and 3:draw line on the MapView won't be executed if one of the if statements is true
    if ([newLocation.timestamp timeIntervalSinceNow] > 3.0) return;
    if (newLocation.horizontalAccuracy > 30.0) return;
    if (newLocation.speed < 0.5) return;
    
    // update distanceTraveled
    if (oldLocation != nil) {
        CLLocationDistance distance = [newLocation distanceFromLocation:oldLocation];
        distanceTraveled += distance;
    }
    
    // formatting output to display distanceTraveled
    int kmPart = distanceTraveled / 1000.0;
    int mPart = ((int)distanceTraveled % (int)1000);
    NSString *string = [NSString stringWithFormat:@"%d.%d km",kmPart, mPart];
    self.distanceLabel.text = string;
    NSLog(@"%@", string);
    
    /*
    // create a NSFileHandle
    fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
    if (!fileHandle) {
        NSLog(@"failed to create a FileHandle");
        return;
    }
    if(!firstWrite){
        [fileHandle seekToEndOfFile];
    }
    */
    
    double oldLati = [oldLocation coordinate].latitude;
    double oldLong = [oldLocation coordinate].longitude;
    double newLati = [newLocation coordinate].latitude;
    double newLong = [newLocation coordinate].longitude;
    // const double EPSILON = 0.000003;
    
    //NSString *newCoord = [NSString stringWithFormat:@"%f %f\n", newLati, newLong];
    //NSData *data2 = [NSData dataWithBytes:newCoord.UTF8String
                                   //length:newCoord.length];
    
    // write the data
    //[fileHandle writeData:data2];
    
    // this line is same as flush in java
    //[fileHandle synchronizeFile];
    
    firstWrite = FALSE;
    
    // Output latitude and longitude
    NSLog(@"didUpdateToLocation latitude=%f, longitude=%f", newLati,
          newLong);
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([newLocation coordinate], 1500, 1500);
    //self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = YES;
    [self.mapView setCenterCoordinate:[newLocation coordinate]];
    [self.mapView setRegion:region];
    //[mapView setRegion:region animated:YES];
    //[mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
    
    CLLocationCoordinate2D coordinates[2];
    CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake(newLati, newLong);
    CLLocationCoordinate2D coord2 = CLLocationCoordinate2DMake(oldLati, oldLong);
    coordinates[0] = coord1;
    coordinates[1] = coord2;
    
    
    
    if(newLocation != nil && oldLocation != nil){
        MKPolyline *line = [MKPolyline polylineWithCoordinates:coordinates
                                                         count:2];
        [mapView addOverlay:line];
    }
    oldLocation = newLocation;
}


// This method is called when the user doesn't allow app to access to his/her location information.
// Also, it might be called when it fails to measure the coordinate for a new location.
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError");
}


#pragma mark - Drawing on Map
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay{
    MKPolylineView *view = [[MKPolylineView alloc]initWithOverlay:overlay];
    view.strokeColor = [UIColor blueColor];
    view.lineWidth = 5.0;
    return view;
}

/*
#pragma mark - Added Method since ver 1.00
- (void)fileWriteSetup {
    // create a home directory
    NSString *homeDir = NSHomeDirectory();
    // create a file path for file output
    filePath = [homeDir stringByAppendingPathComponent:@"coordinates.txt"];
    NSLog(@"%@\n\n", filePath);
    // create a file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
 
     //if(firstWrite){
     //[fileManager createFileAtPath:filePath contents:[NSData data] attributes:nil];
     //}
    
    // if coordinate.txt already exists, it will delete the file
    if([fileManager fileExistsAtPath:filePath])
    {
        [fileManager removeItemAtPath:filePath error:nil];
    }
    
    // if coordinate.txt doesn't exist, it will create such a file
    // if it already exists, this 'if statement' doesn't get executed
    if (![fileManager fileExistsAtPath:filePath]) {
        BOOL result = [fileManager createFileAtPath:filePath
                                           contents:[NSData data] attributes:nil];
        if (!result) {
            NSLog(@"failed to create a file");
            return;
        }
    }
 
     // create a NSFileHandle
     //fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
     //if (!fileHandle) {
     //NSLog(@"failed to create a FileHandle");
     //return;
     //}
}*/


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Trivial Methods
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


#pragma mark - Timer Function 


- (void)timerStart {
    duration = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                target:self
                                              selector:@selector(timer)
                                              userInfo:nil
                                               repeats:YES
                ];
    
    // Refreshes labels for song and bpm information
    [NSTimer scheduledTimerWithTimeInterval:0.2
                                     target:self
                                   selector:@selector(refreshsongbpm)
                                   userInfo:nil
                                    repeats:YES];
}



- (void)timer{
    timerCounter = timerCounter + 1; // increment time every second
    int second = timerCounter % 60; // extract second
    int minute;
    if(timerCounter <= 3600) {
        minute = timerCounter / 60; // extract minute
    }
    else {
        minute = (timerCounter / 60) % 60;
    }
    int hour = timerCounter / 3600; // extract hour
    
    // formatting output to display duration (time elapsed)
    NSString *str = [NSString stringWithFormat:@"%02d:%02d:%02d",hour ,minute ,second];
    self.durationLabel.text = str;
}


#pragma mark - Methods triggered by buttons


- (IBAction)stopActivity:(id)sender {
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
    self.mapView = nil;
    [musicPlayer pause];
}

// Start and stop timer for the run
- (IBAction)startPause:(id)sender {
    if(!isPaused)
    {
        //mapView.showsUserLocation = NO; // if you want to stop showing user location
        // No need to stopUpadatingLocation because when a user hits the pause button,
        // it is assumed that the user won't significantly change his/her lcoation.
        // Thus, the location coordinates won't be updated
        // as it is updated only when a significant change in location occurs
        
        [duration invalidate];
        isPaused = YES;
    }
    else
    {
        //mapView.showsUserLocation = YES;
        //[self.locationManager startUpdatingLocation];
        
        [self timerStart];
        isPaused = NO;
    }
}

// Play and pause music
- (IBAction)playPause:(id)sender
{
    if ([musicPlayer playbackState] == MPMusicPlaybackStatePlaying) {
        [musicPlayer pause];
        
    } else {
        [musicPlayer play];
        
    }
}

- (IBAction)previousSong:(id)sender
{
    [musicPlayer skipToPreviousItem];
    //[self refreshsongbpm];
}


- (IBAction)nextSong:(id)sender
{
    [musicPlayer skipToNextItem];
    //[self refreshsongbpm];
}

//- (IBAction)previousSong:(id)sender;
//- (IBAction)playPause:(id)sender;
//- (IBAction)nextSong:(id)sender;









@end
