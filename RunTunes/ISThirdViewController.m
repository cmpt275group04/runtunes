//
//  ISThirdViewController.m
//  RunTunes
//
//  Created by Wes Anderson on 2014-11-07.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "ISThirdViewController.h"
#import "TempPedometerData.h"
#import "UserProfile.h"
#import "Parameters.h"
#import "Pace.h"

@interface ISThirdViewController ()

@property (weak, nonatomic) IBOutlet UILabel *targetCadence;
@property (weak, nonatomic) IBOutlet UILabel *averageCadence;
@property (weak, nonatomic) IBOutlet UILabel *averagePace;
@property (weak, nonatomic) IBOutlet UILabel *secondsRemainingDisplay; // The on screen seconds countdown
@property (weak, nonatomic) IBOutlet UILabel *secondsRemainingCaption; // The caption below the seconds countdown display
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property int secondsRemainingCount; // variable to hold the number of seconds remaining in the current cycle
@property int cycleLengthInSeconds; // Property to indicate the length of each cycle
@property BOOL countdownFinished;
@property BOOL timerActive;
@property int cycleCount;
@property NSArray *testBPM;
@property NSURL *audioFile;
@property AVAudioPlayer *audio;
@property Pedometer *pedometer;
@property (nonatomic, assign) double totalDistance; // used to track the distance travelled in the current cycle
@property NSDate *distanceMeasurementStartTime;
@property NSDate *distanceMeasurementEndTime;
//@property CLLocationManager *locationManager;
@property TempPedometerData *tempData; // used as a data structure to hold the current cycle's pedometer data
@property Pace *pace;
@property (strong, nonatomic) CLLocation *oldLocation;

@end



@implementation ISThirdViewController

@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    if (self) {
        // Initialize Variables
        self.cycleLengthInSeconds = paramCycleLengthInSeconds;
        self.cycleCount = 0;
        self.secondsRemainingCount = self.cycleLengthInSeconds;
        self.targetCadence.text = [NSString stringWithFormat:@"%i BPM", BPMArray[0]];
        self.averageCadence.text = @"";
        self.averagePace.text = @"";
        self.secondsRemainingDisplay.text = [NSString stringWithFormat:@"%i", self.cycleLengthInSeconds];
        [self.button setTitle:@"Begin first cycle" forState:UIControlStateNormal];
        self.countdownFinished = NO;
        self.tempData = [[TempPedometerData alloc] init];
        self.pedometer = [[Pedometer alloc] init];
        self.pace = [[Pace alloc] initFromUserProfile];
        self.totalDistance = 0.0;
        self.oldLocation = nil;

        // Initialize ManagedObjects
        //NSEntityDescription *entity = [NSEntityDescription entityForName:@"Pedometer" inManagedObjectContext:self.managedObjectContext]; // No longer saved to CoreData, See RunProfile instead
        
        // Load "click.wmv" sound effect file to an url string
        self.audioFile = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                 pathForResource:@"click"
                                                 ofType:@"wav"]];
        
        // Pass the "click.wmv" file to object "audio"
        self.audio = [[AVAudioPlayer alloc]initWithContentsOfURL:self.audioFile error:nil];
        self.audio.delegate = self;
        self.audio.numberOfLoops = 0;
        
        // Grab managedObjectContext from AppDelegate
        self.managedObjectContext = ((AppDelegate *) [UIApplication sharedApplication].delegate).managedObjectContext;
        
        // Hide the back button
        self.navigationItem.hidesBackButton = YES;
        
        // CoreLocation Code
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter=kCLDistanceFilterNone;
        //locationManager.distanceFilter = 10;
        //[locationManager startMonitoringSignificantLocationChanges];

        
        // check if the location service is available
        if ([CLLocationManager locationServicesEnabled]) {
            self.locationManager.delegate = self; // set up a delegate for locationManager
            
            // start mearsurement for coordinates
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestAlwaysAuthorization];
            }
            [self.locationManager startUpdatingLocation];
            
        }
        else {
            NSLog(@"Location services not available.");
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)button:(id)sender {
    // If in the initial setup
    if (self.cycleCount < BPMArrayCount){
        // Start on screen countdown timer
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countdownTimer:) userInfo:nil repeats:YES];
        
        // Start pedometer and location updates
        [self.pedometer startLivePedometerUpdatesToTemp:self.tempData];
        [locationManager startUpdatingLocation];
        
        // Make the necessary label changes
        self.targetCadence.text = [NSString stringWithFormat:@"%i BPM", BPMArray[self.cycleCount]];
        [self.button setHidden:YES];
        
        // Start click sound
        [self playSoundAtInterval:BPMArray[self.cycleCount]];
        
        // Update cycleCount
        self.cycleCount++;
    }
    else{
        // Segue to next page
        [self performSegueWithIdentifier:@"endInitialSetupSegue" sender:sender];
    }

}


// This method is used to perform the on screen countdown timer.  It iterates 30 times until the secondsRemainingCount is zero
// at which point it invalidates itself.
- (void)countdownTimer:(NSTimer *)timer {
    if (self.secondsRemainingCount > 0){ // Timer is in progress
        // Set on screen display to new time remaining
        self.secondsRemainingCount--;
        self.secondsRemainingDisplay.text = [NSString stringWithFormat:@"%i", self.secondsRemainingCount];
        
        // Calculate the average cadence and update label
        double averageCadenceValue = [self.pedometer calculateAverageCadenceFromTemp:self.tempData];
        if (averageCadenceValue <= 0){
            self.averageCadence.text = @"Calculating...";
        }
        else{
            self.averageCadence.text = [NSString stringWithFormat:@"%d steps/min", (int)(averageCadenceValue+0.5)];
        }
        
        // Calculate the average pace and update label
        double averageSpeed = [self calculateAverageSpeed];
        if (averageSpeed <= 0){
            self.averagePace.text = @"Calculating...";
        }
        else{
            self.averagePace.text = [self.pace displayPaceFromSpeedAsStringWithUnits:averageSpeed];
        }
    }
    else{ // Timer has expired
        // Set the countdownFinished flag – used in playClick:(NSTimer *)timer to stop the sound effect from looping
        self.countdownFinished = YES;
        
        // Stop on screen countdown timer
        [timer invalidate];
        
        // Stop pedometer updating
        [self.pedometer stopPedometer];
        
        
        [locationManager stopUpdatingLocation];
        
        // Update the UserProfile-->RunProfile with the pedometer data stored in self.tempData
        [self updateRunProfile];
        
        if (self.cycleCount < BPMArrayCount){ // Another iteration is necessary
            // Reset the secondsRemainingCount variable and on screen display of seconds remaining
            self.secondsRemainingCount = self.cycleLengthInSeconds;
            self.secondsRemainingDisplay.text = [NSString stringWithFormat:@"%d",self.secondsRemainingCount];
            
            [self.button setTitle:@"Begin next cycle" forState:UIControlStateNormal];
            [self.button setHidden:NO];
        }
        else { // The intial setup is complete
            [self.secondsRemainingDisplay setText:@"Finished"];
            
            [self.button setTitle:@"Next" forState:UIControlStateNormal];
            [self.button setHidden:NO];
            [self.secondsRemainingCaption setHidden:YES];
            
        }
        
        
        
        
        
        UserProfile *testUP = [[UserProfile alloc] initFromExistingUserProfile]; // Grab the saved UserProfile
        RunProfile *testRP = testUP.runProfile; // Extract the RunProfile from within the UserProfile
        
        for (NSString* dictionaryKey in testRP.cadenceTable){
            CadenceEntry *testing = [testRP.cadenceTable valueForKey:dictionaryKey];
            NSLog(@"%@  |  %@, %@", dictionaryKey, testing.stepCount, testing.averagePace);
        }
    }
}

// Function to simplify call to NSTimer and make code more readable.  Input the desired bpm of the sound playback and the timer fires accordingly
-(void)playSoundAtInterval:(int)bpm{
    [NSTimer scheduledTimerWithTimeInterval:(60.0/(double)bpm) target:self selector:@selector(playClick:) userInfo:nil repeats:YES];
}

// This method plays the click sound once each time it is called.  The calling frequency depends on the timer
// withTimeInterval: parameter.  The time is invalidated once the countdownTimer method sets the countdownFinished flag to YES
-(void)playClick:(NSTimer *)timer {
    [self.audio play];
    
    if (self.countdownFinished){
        [timer invalidate];
        self.countdownFinished = NO;
    }
}


// This method is called after a completed run to update the run profile with the just completed activity/setup data
-(void)updateRunProfile{
    // Get average cadence from just-completed cycle
    double cycleAverageCadence = [self.pedometer calculateAverageCadenceFromTemp:self.tempData];
        
    // If cycleAverageCadence is a valid number
    if(cycleAverageCadence >= MIN_CADENCE && cycleAverageCadence < MAX_CADENCE){
        // Get userProfile and update RunProfile entity
        UserProfile *user = [[UserProfile alloc] initFromExistingUserProfile]; // Grab the saved UserProfile
        RunProfile *updatedRunProfile = user.runProfile; // Extract the RunProfile from within the UserProfile
        
        // Determine the correct cadence span to store the value in
        NSString *dictionaryKey = nil;
        
        // For each entry in the cadenceTable NSDicitonary
        for (id key in updatedRunProfile.cadenceTable){
            // "nextKey" is the cadence value in NSString* format
            //NSNumber *currentKeyValue = [NSNumber numberWithInt:(int)[key integerValue]];
            int keyIntegerValue = (int)[key integerValue];
            
            double lowerBound = keyIntegerValue - paramCadenceEntrySpan/2.0;
            double upperBound = keyIntegerValue + paramCadenceEntrySpan/2.0;
            
            // Check to see if the current entry is the correct place to update the info from the latest cycle
            if (cycleAverageCadence >= lowerBound && cycleAverageCadence < upperBound){
                // If so, update the dictionaryKey variable
                dictionaryKey = key;
                break; // exit loop
            }
        }
        
        // Check for out of bound error
        if(dictionaryKey == nil){
            NSLog(@"ERROR: Average cycle cadence outside of [MIN_CADENCE, MAX_CADENCE)");
            exit(EXIT_FAILURE);
        }
        
        // Extract the cadenceEntry structure for the appropriate entry in the cadenceTable
        CadenceEntry *entryToUpdate = [updatedRunProfile.cadenceTable objectForKey:dictionaryKey];
        
        // Get values from last cycle
        int stepsTakenDuringCycle = [self.pedometer calculateStepsTakenFromTemp:self.tempData];
        double averagePaceDuringCycle = [self.pace displayPaceFromSpeed:[self calculateAverageSpeed]];
        int previousStepTotal;
        double previousAveragePace;
        
        // If this entry in the cadenceTable is empty
        if (entryToUpdate.stepCount == nil || entryToUpdate.averagePace == nil){
            previousStepTotal = 0;
            previousAveragePace = 0;
        }
        else{
            previousStepTotal = (int)[entryToUpdate.stepCount integerValue];
            previousAveragePace = [entryToUpdate.averagePace doubleValue];
        }
        
        // Update the cadenceEntry structure
        int newStepTotal = stepsTakenDuringCycle + previousStepTotal;
        
        double newAveragePace;
        if(newStepTotal > 0){
            newAveragePace = ((double)previousStepTotal/(double)newStepTotal)*previousAveragePace + ((double)stepsTakenDuringCycle/(double)newStepTotal)*averagePaceDuringCycle;
        }
        else{
            NSLog(@"A fetal error occurred while updating the RunProfile");
            exit(EXIT_FAILURE);
        }
        entryToUpdate.averagePace = [NSNumber numberWithDouble:newAveragePace];
        entryToUpdate.stepCount = [NSNumber numberWithInt:newStepTotal];
        
        // Clear distance variables
        self.distanceMeasurementStartTime = nil;
        self.distanceMeasurementEndTime = nil;
        self.totalDistance = 0;
        
        // Update the CoreData Entity
        [updatedRunProfile.cadenceTable setObject:entryToUpdate forKey:dictionaryKey];
        [user setRunProfile:updatedRunProfile];
        [user updateCoreDataEntry];
        
        
        UserProfile *testUP = [[UserProfile alloc] initFromExistingUserProfile]; // Grab the saved UserProfile
        RunProfile *testRP = testUP.runProfile; // Extract the RunProfile from within the UserProfile

        CadenceEntry *testing = [testRP.cadenceTable valueForKey:dictionaryKey];
        NSLog(@"%@  |  %@, %@", dictionaryKey, testing.stepCount, testing.averagePace);
        
        
        
        
        
        // Wipe existing TempPedometerData object and initialize a new one
        self.tempData = nil;
        self.tempData = [[TempPedometerData alloc] init];
    } // end if
}

// The method to measure the distance.
// Since the device test is not done to this method, I don't know how accurate it is.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *newLocation = [locations lastObject];
    
    //NSLog(@"Location Update Method was called");
    if ([newLocation.timestamp timeIntervalSinceNow] > 3.0) return;
    if (newLocation.horizontalAccuracy > 30.0) return;
    if (newLocation.speed < 0.5) return;
    
    if (self.oldLocation != nil) {
        CLLocationDistance distance = [newLocation distanceFromLocation:self.oldLocation];
        self.totalDistance += distance;
        self.distanceMeasurementEndTime = [NSDate date];
    }
    
    // Timestamp the initial distance measurement to be used later for calculated average speed over the cycle
    if(self.distanceMeasurementStartTime == nil){
        self.distanceMeasurementStartTime = [NSDate date];
    }
    //self.oldLocation = [[CLLocation alloc] init];
    self.oldLocation = newLocation;
}

- (double)calculateAverageSpeed{
    NSTimeInterval durationMinutes = [self.distanceMeasurementEndTime timeIntervalSinceDate:self.distanceMeasurementStartTime]/60;
    int averageSpeed = self.totalDistance/durationMinutes;
        
    return averageSpeed;
}


 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Set UserProfile isConfigured BOOL to true
     UserProfile *user = [[UserProfile alloc] initFromExistingUserProfile];
     
     // Update the isConfigured BOOL
     user.isConfigured = [NSNumber numberWithBool:YES];
     
     // Update the CoreData entry
     [user updateCoreDataEntry];
 }


@end
