//
//  PedometerFunctionality.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "PedometerFunctionality.h"

@implementation PedometerFunctionality

- (void)startLivePedometerUpdates{
    // This block of code instantiates a new managedObject to hold the pedometer data and updates it with the live
    // updates provided by CoreMotion.  Further processing to determine overall and instantaeous cadence is calcuated
    // in -(double)calculateAverageCadence:fromDate and -(double)calculateInstantaneousCadence.
    
    
    // Instantiate the NSManagedObject to hold necessary persistent data
    PedometerData* myMO = (PedometerData *)[NSEntityDescription insertNewObjectForEntityForName:@"PedometerData" inManagedObjectContext:[self managedObjectContext]];
    
    // Start pedometer and store data in managedObjectContext
    [self.pedometer startPedometerUpdatesFromDate:[NSDate date]
                                      withHandler:^(CMPedometerData *pedometerData, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              if(error!=nil){ // check for errors getting pedometer update
                                                  NSLog(@"Error occurred! %@", error);
                                              }
                                              else{
                                                  // update CoreData Pedometer Entity with latest reading from CoreMotion
                                                  [myMO setTimeStamp:[NSDate date]];
                                                  myMO.stepCount = pedometerData.numberOfSteps;
                                                  
                                                  // Save new data in managedObjectContext and report error if one occurred
                                                  NSError *saveError = nil;
                                                  
                                                  if([[self managedObjectContext]save:&saveError]){
                                                      NSLog(@"Error occured: %@", saveError);
                                                  }
                                              }
                                              
                                          });
                                      }];
}

- (void)stopPedometer:(id)sender {
    [self.pedometer stopPedometerUpdates];
}

- (double)calculateAverageCadence:(NSDate *)fromDate{
    // Setup date boundaries
    NSDate *oldDate = fromDate;
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *startComponents = [calendar components:unitFlags fromDate:oldDate];
    NSDateComponents *endComponents = [calendar components:unitFlags fromDate:oldDate];
    
    startComponents.hour   = 00;
    startComponents.minute = 00;
    startComponents.second = 00;
    endComponents.hour = 23;
    endComponents.minute = 59;
    endComponents.second = 59;
    
    NSDate *startDate = [calendar dateFromComponents:startComponents];
    NSDate *endDate = [calendar dateFromComponents:endComponents];
    
    // Fetch data from last run
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Pedometer" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    // Specify criteria for filtering which objects to fetch
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeStamp > '%@*'",startDate, @"timeStamp <= '%@'", endDate];
    [fetchRequest setPredicate:predicate];
    
    // Specify how the fetched objects should be sorted
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    // Execute fetch and check for errors
    NSError *error = nil;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"Error occured: %@", error);
    }
    
    // Calculate average step count for all entries in the Pedometer CoreData entity from fromDate
    // Determine step count values for first and last entry of entity
    int startPedStepCount = [[fetchedObjects[0] stepCount] intValue]; // determine original step count
    int endPedStepCount = [[fetchedObjects[[fetchedObjects count] - 1] stepCount] intValue]; // determine end step count
    
    // Determine number of minutes between first and last entity entries
    NSDate *startPedDate = [fetchedObjects[0] timeStamp];
    NSDate *endPedDate = [fetchedObjects[[fetchedObjects count] - 1] timeStamp];
    double minutes = [startPedDate timeIntervalSinceDate:endPedDate]/60;
    
    return (endPedStepCount - startPedStepCount) / minutes; // calculate and return average cadence (steps/minute)
}

// Calculates the average cadence.  The withSensitivity value is given in seconds and indicates the number of seconds over which
// to calculate the average.  withSensitivity should be >= 5.
- (double)calculateInstantaneousCadence:(int)withSensitivity{
    int timeToleranceInSeconds;
    
    if(withSensitivity < 5){
        timeToleranceInSeconds = 5; // minimum value to ensure data available
    }
    else{
        timeToleranceInSeconds = withSensitivity;
    }
    
    // Setup date boundaries
    NSDate *now = [NSDate date];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *startComponents = [calendar components:unitFlags fromDate:now];
    
    startComponents.hour   = 00;
    startComponents.minute = 00;
    // *** Probably buggy due to the posibility of negative seconds.  Use an NSDate method instead ***
    startComponents.second = startComponents.second - timeToleranceInSeconds;
    
    NSDate *startDate = [calendar dateFromComponents:startComponents];
    
    // Fetch data from last run
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Pedometer" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    // Specify criteria for filtering which objects to fetch
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeStamp > '%@*'",startDate, @"timeStamp <= '%@'", now];
    [fetchRequest setPredicate:predicate];
    
    // Specify how the fetched objects should be sorted
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    // Execute fetch and check for errors
    NSError *error = nil;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"Error occured: %@", error);
    }
    
    // Calculate average step count for all entries in the Pedometer CoreData entity from fromDate
    // Determine step count values for first and last entry of entity
    int startPedStepCount = [[fetchedObjects[0] stepCount] intValue]; // determine original step count
    int endPedStepCount = [[fetchedObjects[[fetchedObjects count] - 1] stepCount] intValue]; // determine end step count
    
    // Determine number of minutes between first and last entity entries
    NSDate *startPedDate = [fetchedObjects[0] timeStamp];
    NSDate *endPedDate = [fetchedObjects[[fetchedObjects count] - 1] timeStamp];
    double minutes = [startPedDate timeIntervalSinceDate:endPedDate]/60;
    
    return (endPedStepCount - startPedStepCount) / minutes; // calculate and return instantaneous cadence (steps/minute)
}

@end
