//
//  Pace.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-24.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pace : NSObject

- (id)initFromUserProfile;

// converts the speed from meter/minute to the user's desired measure of pace
- (double)displayPaceFromSpeed:(double)speed;
- (NSString *)displayPaceFromSpeedAsStringWithUnits:(double)speed;


@end
