//
//  Parameters.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-19.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <Foundation/Foundation.h>

int const MIN_CADENCE = 100;
int const MAX_CADENCE = 200;

int const paramCadenceEntrySpan = 5;
int const paramCycleLengthInSeconds = 3;

//int BPMArray[] = {120, 135, 150, 165};
//int BPMArrayCount = 4;
int BPMArray[] = {120, 135};
int BPMArrayCount = 2;