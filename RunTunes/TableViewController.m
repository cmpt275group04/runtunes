//
//  TableViewController.m
//  TableViewTest
//
//  Created by luke zeng on 2014-11-16.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Helps"];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell4" forIndexPath:indexPath];
    
    NSArray *sections =[NSArray arrayWithObjects:@"FAQ", @"Tips", @"Report an Error", nil];
    cell.textLabel.text = sections[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"FAQID" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"TipsID" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"RepErrID" sender:self];
        default:
            break;
    }
    //[self performSegueWithIdentifier:sections[indexPath.row] sender:self]; ??
}

@end
