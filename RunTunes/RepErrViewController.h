//
//  RepErrViewController.h
//  TableViewTest
//
//  Created by luke zeng on 2014-11-16.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 

@interface RepErrViewController : UIViewController <MFMailComposeViewControllerDelegate>

@end
