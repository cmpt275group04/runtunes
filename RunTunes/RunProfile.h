//
//  RunProfile.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-16.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parameters.h"

/*
@interface RunProfileToDataTransformer : NSValueTransformer { }
@end
 */


/* The DataEntry object is used as the primary object through which to store data in the RunProfile object.
 The average running speed of the user at a given cadence is recorded along with the number of steps from which that
 average was calculated (needed to accurately weight new data - see ____).
 
 Each instance of the DataEntry object represents once such cadence<-->averageSpeed relation.  At the moment, the degree of
 precision regarding how many such cadence<-->averageSpeed realtions is unknown.  In testing, this will be set by the range
 property in the RunProfile object.
*/

@interface CadenceEntry : NSObject

@property NSNumber *stepCount;
@property NSNumber *averagePace;

@end

/*typedef struct {
    int stepCount;
    double averageSpeed;
} cadenceEntry;*/

@interface RunProfile : NSObject <NSCoding>

@property NSNumber *entrySpan; // The span of steps/minute to group together.  Higher value --> more resource intensive, more accurate
@property NSMutableDictionary *cadenceTable;

- (id)initWithSpan:(int)span;

@end