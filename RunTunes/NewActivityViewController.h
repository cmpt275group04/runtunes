//
//  NewActivityViewController.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <UIKit/UIKit.h>

struct dataPoint{
    double XCoordinateCadence;
    double YCoordinatePace;
};

// y = mx + b
struct lineEquation{
    double m;
    double b;
};

@interface NewActivityViewController : UIViewController

@end
