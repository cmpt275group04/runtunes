//
//  FAQsViewController.m
//  TableViewTest
//
//  Created by luke zeng on 2014-11-16.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "FAQViewController.h"

@interface FAQViewController ()

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"FAQ"];
    
    UIView *contentView;
    
    contentView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,1700)];
    
    [myScroller addSubview:contentView];
    
    [myScroller setScrollEnabled:YES];
    [myScroller setContentSize:CGSizeMake(320, 1700)];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
