//
//  EndRunViewController.m
//  RunTunes
//
//  Created by Paul Khuu on 2014-11-16.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "EndRunViewController.h"
#import <Social/Social.h>



@interface EndRunViewController ()



@end

@implementation EndRunViewController

//@synthesize hostView;

/* Action taken when the "Save" button (saveAsImageButton) is pressed in the app */
- (IBAction)saveScreenshot {
    
    // Define the dimensions of the screenshot you want to take (the entire screen in this case)
    CGSize size =  [[UIScreen mainScreen] bounds].size;
    
    // Create the screenshot
    UIGraphicsBeginImageContext(size);
    // Put everything in the current view into the screenshot
    [[self.view layer] renderInContext:UIGraphicsGetCurrentContext()];
    // Save the current image context info into a UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Save the screenshot to the device's photo album
    UIImageWriteToSavedPhotosAlbum(newImage, self,
                                   @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    // Create path.
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"twitter.png"];
    
    // Save image for attachment
    //[UIImagePNGRepresentation(newImage) writeToFile:filePath atomically:YES];
}

// callback for UIImageWriteToSavedPhotosAlbum
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    if (error) {
        // Handle if the image could not be saved to the photo album
    }
    else {
        // The save was successful and all is well
    }
}

// Captures current view as UIImage
- (UIImage*)captureView:(UIView *)view
{
    CGRect rect = [[UIScreen mainScreen] bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (IBAction)postToTwitter:(id)sender {
    // Verify if Twitter is accessible
    //if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        // Create instance of SLComposeViewController if available
        
        
        SLComposeViewController *tweetSheet = [SLComposeViewController
                            composeViewControllerForServiceType:SLServiceTypeTwitter];
        // Add text and image with run statistics
        [tweetSheet setInitialText:@"I just went for a run using RunTunes!"];
        // Attach screenshot of current view
        [tweetSheet addImage:[self captureView:self.view]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

- (IBAction)postToFacebook:(id)sender {
    // Verify if Facebook is accessible
   // if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        // Create instance of SLComposeViewController if available
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        // Add text and image with run statistics
        [controller setInitialText:@"I just went for a run using RunTunes!"];
        // Attach screenshot of current view
        [controller addImage:[self captureView:self.view]];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.view.frame = view1.frame;
    
    // set up host view for drawing graph
    CPTGraphHostingView *hostView  = [[CPTGraphHostingView alloc] initWithFrame:CGRectMake(0, 87, 428, 340)];
    [self.view addSubview: hostView];
    
    //hostView.backgroundColor=[UIColor greenColor];
    
    // Create a CPTGraph object and add to hostView
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:CGRectMake(0, 87, 428, 340)];
    graph.plotAreaFrame.masksToBorder = NO;
    graph.paddingLeft = 55.0;
    graph.paddingRight = 10.0;
    graph.paddingTop = 10.0;
    graph.paddingBottom = 35.0;
    
    // Themes to set up (choose one of them)
    //[graph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];
    //[graph applyTheme:[CPTTheme themeNamed:kCPTSlateTheme]];
    //[graph applyTheme:[CPTTheme themeNamed:kCPTStocksTheme]];
    [graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.lineColor = [CPTColor whiteColor];
    lineStyle.lineWidth = 2.0f;
    
    // set up x-axis of the graph
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromString(@"5");
    x.minorTicksPerInterval = 4;
    x.majorTickLineStyle = lineStyle;
    x.minorTickLineStyle = lineStyle;
    x.axisLineStyle = lineStyle;
    x.minorTickLength = 5.0f;
    x.majorTickLength = 9.0f;
    x.title = @"Time Elapsed (minutes)";
    x.titleOffset = 20.0f; // move the title to bottom. positive values make it move to the top
    
    // set up y-axis of the graph
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength = CPTDecimalFromString(@"4");
    y.minorTicksPerInterval = 3;
    y.majorTickLineStyle = lineStyle;
    y.minorTickLineStyle = lineStyle;
    y.axisLineStyle = lineStyle;
    y.minorTickLength = 5.0f;
    y.majorTickLength = 9.0f;
    y.title = @"Pace (meters/bpm) or Speed (meters/minutes)";
    y.titleOffset = 35.0f;	//	move left to y axis. negative values make it move to the right
    lineStyle.lineWidth = 0.5f;
    y.majorGridLineStyle = lineStyle;
    
    // add the graph to the hostView
    hostView.hostedGraph = graph;
    
    // Get the (default) plotspace from the graph so we can set its x/y ranges
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    // Note that these CPTPlotRange are defined by START and LENGTH (not START and END) !!
    //[plotSpace setYRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(16)]];
    //[plotSpace setXRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-4) length:CPTDecimalFromFloat(8)]];
    [plotSpace setYRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(32)]];
    [plotSpace setXRange: [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(16)]];
    
    // Create the plot (we do not define actual x/y values yet, these will be supplied by the datasource...)
    CPTScatterPlot *plot = [[CPTScatterPlot alloc] initWithFrame:CGRectZero];
    
    
    // Let's keep it simple and let this class act as datasource (therefore we implement <CPTPlotDataSource>)
    plot.dataSource = self;
    
    // Finally, add the created plot to the default plot space of the CPTGraph object we created before
    [graph addPlot:plot toPlotSpace:graph.defaultPlotSpace];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// This method is here because this class also functions as datasource for our graph
// Therefore this class implements the CPTPlotDataSource protocol
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plotnumberOfRecords {
    // plot 16 'points'
    return 15 + 1;
}


// This method is here because this class also functions as datasource for our graph
// Therefore this class implements the CPTPlotDataSource protocol
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    // We need to provide an X or Y (this method will be called for each) value for every index
    int x = (int)index;
    
    //NSLog(@"%d", index);
    NSLog(@"%d", x);
    
    // This method is actually called twice per point in the plot, one for the X and one for the Y value
    if(fieldEnum == CPTScatterPlotFieldX)
    {
        // Return x value, which will, depending on index, be between -4 to 4
        return [NSNumber numberWithInt: x];
    } else {
        // Return y value, for this example we'll be plotting y = x * x
        return [NSNumber numberWithInt: x * 2];
    }
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
