//
//  Pace.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-24.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "Pace.h"
#import "UserProfile.h"

@interface Pace ()
@property UserProfile *user;
@end

@implementation Pace

- (id)initFromUserProfile{
    self.user = [[UserProfile alloc] initFromExistingUserProfile];
    
    return self;
}

// This function receives in input speed in meters/minute and outputs either min/km or min/mile
- (NSString *)displayPaceFromSpeedAsStringWithUnits:(double)speed{
    double pace;
    NSString *string;
    
    if([self.user.prefersMinPerKmPace isEqualToNumber:[NSNumber numberWithBool:YES]]){
        pace = 1/speed * 1000; // conversion to min/km
        string = [NSString stringWithFormat:@"%.2f min/km", pace];
    }
    else{
        pace = 1/speed * 1609.344; // conversion to min/mile
        string = [NSString stringWithFormat:@"%.2f min/mile", pace];
    }
    
    return string;
}

- (double)displayPaceFromSpeed:(double)speed{
    double pace;
    
    if([self.user.prefersMinPerKmPace isEqualToNumber:[NSNumber numberWithBool:YES]]){
        pace = 1/speed * 1000; // conversion to min/km
    }
    else{
        pace = 1/speed * 1609.344; // conversion to min/mile
    }
    
    return pace;
}

@end
