//
//  PedometerFunctionality.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-09.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

@import CoreData; // used for persistent storage
@import CoreMotion; // Used for pedometer functionality
#import <Foundation/Foundation.h>
#import "AppDelegate.h" // Used to pass managedObjectContext from AppDelegate

@interface PedometerFunctionality : NSObject

@property (nonatomic,strong) CMPedometer* pedometer;
@property (nonatomic) NSInteger* currentCadence;
@property (nonatomic) NSDate* timeOfLastQuery;
//@property (weak, nonatomic) IBOutlet UILabel *pedometerStatusLabel;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (void)startLivePedometerUpdatesWithManagedObjectContext:(NSManagedObjectContext *)moc;
- (double)calculateAverageCadence:(NSDate *)fromDate;
- (double)calculateInstantaneousCadence:(int)withSensitivity;

@end
