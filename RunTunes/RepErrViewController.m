//
//  RepErrViewController.m
//  TableViewTest
//
//  Created by luke zeng on 2014-11-16.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "RepErrViewController.h"

@interface RepErrViewController ()

@end

@implementation RepErrViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //try if sending email is enabled on the device
    if([MFMailComposeViewController canSendMail])
     {
    //make an email object for error reporting
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc]init];
    mailController.mailComposeDelegate = self;
    
    //email subject
    [mailController setSubject:@"Error Report"];
    //email content
    [mailController setMessageBody:@"Error description" isHTML:NO];
    //email recepent
    NSArray *recipent = [NSArray arrayWithObjects:@"RunTunErrRep@someMail.com",nil];
    [mailController setToRecipients:recipent];
    
    //show mail view controller
        [self presentViewController:mailController animated:YES completion:NULL];
    
    }
    
    //output error message
    //@catch(NSException *myException)
    else{
        //NSLog(@"!!!Error: cannot sent email!!!");
        // NSLog(@"!!!Error: %@,", myException.name);
        //NSLog(@"Details: %@,", myException.description);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error!" message:@"Please go to Settings > Mail, and enable your iPhone's email sending"
                                            preferredStyle:UIAlertControllerStyleAlert];
        //ok action
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"ok action")
                                                     style: UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {NSLog(@"ok action");}];
        //alert add "ok"action
        [alert addAction:ok];
        //show alert
        [self presentViewController:alert animated:YES completion:nil];
        
        //goback to previous view
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

        //check if the email feature is working properly
-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Error report cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Error report saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Error report sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Error report sent failure: %@",[error localizedDescription]);
        default:
            break;
    }
    
    //close the email interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
