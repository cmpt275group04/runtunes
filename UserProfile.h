//
//  UserProfile.h
//  RunTunes
//
//  Created by Justin Raine on 2014-11-19.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RunProfile.h"


@interface UserProfile : NSManagedObject

@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * heightInch; // added
@property (nonatomic, retain) NSNumber * isConfigured;
@property (nonatomic, retain) NSNumber * isMale;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * prefersKgWeight;
@property (nonatomic, retain) RunProfile *runProfile;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSNumber * prefersCmHeight;
@property (nonatomic, retain) NSNumber * prefersMinPerKmPace;

- (id)initFromExistingUserProfile;
- (void)updateCoreDataEntry;

@end
