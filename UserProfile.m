//
//  UserProfile.m
//  RunTunes
//
//  Created by Justin Raine on 2014-11-19.
//  Copyright (c) 2014 Simon Fraser University. All rights reserved.
//

#import "UserProfile.h"
#import "AppDelegate.h"


@implementation UserProfile

@dynamic age;
@dynamic firstName;
@dynamic height;
@synthesize heightInch; //// added
@dynamic isConfigured;
@dynamic isMale;
@dynamic lastName;
@dynamic prefersKgWeight;
@synthesize runProfile;
@dynamic weight;
@dynamic prefersCmHeight;
@dynamic prefersMinPerKmPace;

- (id)initFromExistingUserProfile{
    // Retrieve the context
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
    
    moc = ((AppDelegate *) [UIApplication sharedApplication].delegate).managedObjectContext;
    
    // Retrieve the entity from the local store -- much like a table in a database
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Request the data -- NOTE, this assumes only one match, that
    // yourIdentifyingQualifier is unique. It just grabs the first object in the array.
    NSError *error = nil;
    NSArray *fetchedObjects = [moc executeFetchRequest:request error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"Error occured: %@", error);
    }
    
    return fetchedObjects[0];
}

- (void)updateCoreDataEntry{
    // Retrieve the context
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
    
    moc = ((AppDelegate *) [UIApplication sharedApplication].delegate).managedObjectContext;
    
    // Retrieve the entity from the local store -- much like a table in a database
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Request the data -- NOTE, this assumes only one match, that
    // yourIdentifyingQualifier is unique. It just grabs the first object in the array.
    NSError *error = nil;
    NSArray *fetchedObjects = [moc executeFetchRequest:request error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"Error occured: %@", error);
    }
    
    if(![moc save:&error]){
        // If a save error occurred
        NSLog(@"Error occured: %@", error);
    }
}

@end
